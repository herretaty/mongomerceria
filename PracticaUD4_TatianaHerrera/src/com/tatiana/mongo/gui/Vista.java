package com.tatiana.mongo.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.tatiana.mongo.base.Producto;
import com.tatiana.mongo.base.Proveedor;
import com.tatiana.mongo.base.Vendedor;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame{

    private JPanel panelPrincipal;
    JTabbedPane tabbedPane1;
    JTextField txtNombreProducto;
    JTextField txtOrigen;
    JTextField txtPrecio;
    JButton btnAnnadirProducto;
    JButton btnModificarProducto;
    JButton btnEliminarProducto;
    JTextField txtBuscarProducto;
    JList<Producto>listProductos;
    JTextField txtNombreVendedor;
    JTextField txtApellidoVendedor;
    JTextField txtDireccion;
    JList<Producto> listBusquedaProducto;
    JButton btnAnnadirVendedor;
    JButton btnModificarVendedor;
    JButton btnEliminarVendedor;
    JTextField txtbuscarVendedor;
    JList<Vendedor> listVendedores;
    JList<Vendedor> listBusquedaVendedor;
    JTextField txtNombreProveedor;
    JButton btnAnnadirProveedor;
    JButton btnModificarProveedor;
    JButton btnEliminarProveedor;
    JList<Proveedor> listProveedores;
    JList<Proveedor> listBusquedaProveedor;
    JTextField txtBuscarProveedor;
    DatePicker fechaNacimiento;
    JTextField txtReferencia;
    DatePicker fechaReparto;
    JTextField txtcodigoReparto;
    JTextField txtCantidad;


    //MODELOS//
    DefaultListModel<Producto>dlmProductos;
    DefaultListModel<Vendedor>dlmVendedores;
    DefaultListModel<Proveedor>dlmProveedores;

    DefaultListModel<Producto> dlmProductosBusqueda;
    DefaultListModel<Vendedor> dlmVendedorBusqueda;
    DefaultListModel<Proveedor> dlmProveedoresBusqueda;

    //MENU//

    JMenuItem itemConectar;
    JMenuItem itemSalir;

    public Vista(){
        setTitle("Merceria - <SIN CONEXION>");
        setContentPane(panelPrincipal);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(900, 650));
        setResizable(false);
        pack();
        setVisible(true);

        inicializarModelos();
        inicializarMenu();
    }

    private void inicializarMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("conexion");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuArchivo);

        setJMenuBar(menuBar);

    }

    private void inicializarModelos() {

        dlmProductos = new DefaultListModel<>();
        listProductos.setModel(dlmProductos);

        dlmVendedores= new DefaultListModel<>();
        listVendedores.setModel(dlmVendedores);

        dlmProveedores= new DefaultListModel<>();
        listProveedores.setModel(dlmProveedores);

        dlmProductosBusqueda= new DefaultListModel<Producto>();
        listBusquedaProducto.setModel(dlmProductosBusqueda);

        dlmVendedorBusqueda= new DefaultListModel<Vendedor>();
        listBusquedaVendedor.setModel(dlmVendedorBusqueda);

        dlmProveedoresBusqueda= new DefaultListModel<Proveedor>();
        listBusquedaProveedor.setModel(dlmProveedoresBusqueda);


    }

}
