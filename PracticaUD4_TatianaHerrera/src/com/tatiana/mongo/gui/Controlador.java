package com.tatiana.mongo.gui;


import com.tatiana.mongo.Util.Util;
import com.tatiana.mongo.base.Producto;
import com.tatiana.mongo.base.Proveedor;
import com.tatiana.mongo.base.Vendedor;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * @author TatianaHerrera
 */

public class Controlador implements ActionListener, KeyListener, ListSelectionListener {


    private Modelo modelo;
    private Vista vista;

    public Controlador(Modelo modelo, Vista vista) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);

        try {
            modelo.conectar();
            vista.itemConectar.setText("Desconectar");
            vista.setTitle("Merceria - <CONECTADO>");
            setBotonesActivados(true);
            listarProductos();
            listarVendedores();
            listarProveedores();
        } catch (Exception ex) {
            Util.mostrarMensajeError("Imposible establecer conexión con el servidor.");
        }
    }

    /**
     * Metodo que llama a los botones
     * @param listener
     */


    private void addActionListeners(ActionListener listener){
        vista.btnAnnadirProducto.addActionListener(listener);
        vista.btnModificarProducto.addActionListener(listener);
        vista.btnEliminarProducto.addActionListener(listener);
        vista.btnAnnadirVendedor.addActionListener(listener);
        vista.btnModificarVendedor.addActionListener(listener);
        vista.btnEliminarVendedor.addActionListener(listener);
        vista.btnAnnadirProveedor.addActionListener(listener);
        vista.btnModificarProveedor.addActionListener(listener);
        vista.btnEliminarProveedor.addActionListener(listener);

        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    private void addListSelectionListeners(ListSelectionListener listener){
        vista.listProductos.addListSelectionListener(listener);
        vista.listVendedores.addListSelectionListener(listener);
        vista.listProveedores.addListSelectionListener(listener);
    }

    private void addKeyListeners(KeyListener listener){
        vista.txtBuscarProducto.addKeyListener(listener);
        vista.txtbuscarVendedor.addKeyListener(listener);
        vista.txtBuscarProveedor.addKeyListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "conexion":
                try {
                    if (modelo.getCliente() == null) {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        vista.setTitle("Merceria - <CONECTADO>");
                        setBotonesActivados(true);
                        listarProductos();
                        listarVendedores();
                        listarProveedores();
                    } else {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        vista.setTitle("Merceria- <SIN CONEXION>");
                        setBotonesActivados(false);
                        vista.dlmProductos.clear();
                        vista.dlmVendedores.clear();
                        vista.dlmProveedores.clear();
                        limpiarCamposProducto();
                        limpiarCamposVendedor();
                        limpiarCamposProveedor();
                    }
                } catch (Exception ex) {
                    Util.mostrarMensajeError("Imposible establecer conexión con el servidor.");
                }
                break;

            case "salir":
                modelo.desconectar();
                System.exit(0);
                break;

            case "Añadir Producto":
                if (comprobarCamposProducto()) {
                    modelo.guardarObjeto(new Producto(vista.txtNombreProducto.getText(),
                            vista.txtOrigen.getText(),
                            vista.txtReferencia.getText(),
                            Float.parseFloat(vista.txtPrecio.getText()),
                            Integer.parseInt(vista.txtCantidad.getText())));
                    limpiarCamposProducto();
                } else {
                    Util.mostrarMensajeError("No ha sido posible insertar el producto en la base de datos.\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarProductos();
                break;

            case "Modificar Producto":
                if (vista.listProductos.getSelectedValue() != null) {
                    if (comprobarCamposProducto()) {
                        Producto producto = vista.listProductos.getSelectedValue();
                        producto.setNombreProducto(vista.txtNombreProducto.getText());
                        producto.setOrigen(vista.txtOrigen.getText());
                        producto.setReferencia(vista.txtReferencia.getText());
                        producto.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                        producto.setCantidad(Integer.parseInt(vista.txtCantidad.getText()));
                        modelo.modificarObjeto(producto);
                        limpiarCamposProducto();
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el producto en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                    listarProductos();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "Eliminar Producto":
                if (vista.listProductos.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listProductos.getSelectedValue());
                    listarProductos();
                    limpiarCamposProducto();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "Añadir Vendedor":
                if (comprobarCamposVendedor()) {
                    modelo.guardarObjeto(new Vendedor(vista.txtNombreVendedor.getText(),
                            vista.txtApellidoVendedor.getText(),
                            vista.txtDireccion.getText(),
                            vista.fechaNacimiento.getDate()));
                    limpiarCamposVendedor();
                } else {
                    Util.mostrarMensajeError("No ha sido posible insertar el vendedor en la base de datos.\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarVendedores();
                break;

            case "Modificar Vendedor":
                if (vista.listVendedores.getSelectedValue() != null) {
                    if (comprobarCamposVendedor()) {
                        Vendedor vendedor = vista.listVendedores.getSelectedValue();
                        vendedor.setNombreVendedor(vista.txtNombreVendedor.getText());
                        vendedor.setApellidosVendedor(vista.txtApellidoVendedor.getText());
                        vendedor.setDireccion(vista.txtDireccion.getText());
                        vendedor.setFecha_nacimiento(vista.fechaNacimiento.getDate());
                        modelo.modificarObjeto(vendedor);
                        limpiarCamposVendedor();
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el vendedor en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                    listarVendedores();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "Eliminar Vendedor":
                if (vista.listVendedores.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listVendedores.getSelectedValue());
                    listarVendedores();
                    limpiarCamposVendedor();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "Añadir Proveedor":
                if (comprobarCamposProveedor()) {
                    modelo.guardarObjeto(new Proveedor(vista.txtNombreProveedor.getText(),
                            vista.fechaReparto.getDate(),
                            vista.txtcodigoReparto.getText()));
                    limpiarCamposProveedor();
                } else {
                    Util.mostrarMensajeError("No ha sido posible insertar el proveedor en la base de datos.\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarProveedores();
                break;

            case "Modificar Proveedor":
                if (vista.listProveedores.getSelectedValue() != null) {
                    if (comprobarCamposProveedor()) {
                        Proveedor proveedor = vista.listProveedores.getSelectedValue();
                        proveedor.setNombreProveedor(vista.txtNombreProveedor.getText());
                        proveedor.setFechaReparto(vista.fechaReparto.getDate());
                        proveedor.setCodigoReparto(vista.txtcodigoReparto.getText());
                        modelo.modificarObjeto(proveedor);
                        limpiarCamposProveedor();
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el proveedor en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                    listarProveedores();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;

            case "Eliminar Proveedor":
                if (vista.listProveedores.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listProveedores.getSelectedValue());
                    listarProveedores();
                    limpiarCamposProveedor();
                    break;
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscarProducto) {
            listarProductosBusqueda(modelo.getProductos(vista.txtBuscarProducto.getText()));
            if (vista.txtBuscarProducto.getText().isEmpty()) {
                vista.dlmProductosBusqueda.clear();
            }
        } else if (e.getSource() == vista.txtbuscarVendedor) {
            listarVendedorBusqueda(modelo.getVendedores(vista.txtbuscarVendedor.getText()));
            if (vista.txtbuscarVendedor.getText().isEmpty()) {
                vista.dlmVendedorBusqueda.clear();
            }
        } else if (e.getSource() == vista.txtBuscarProveedor) {
            listarProveedoresBusqueda(modelo.getProveedores(vista.txtBuscarProveedor.getText()));
            if (vista.txtBuscarProveedor.getText().isEmpty()) {
                vista.dlmProveedoresBusqueda.clear();
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == vista.listProductos) {
            if (vista.listProductos.getSelectedValue() != null) {
                Producto producto = vista.listProductos.getSelectedValue();
                vista.txtNombreProducto.setText(producto.getNombreProducto());
                vista.txtOrigen.setText(producto.getOrigen());
                vista.txtReferencia.setText(producto.getReferencia());
                vista.txtPrecio.setText(String.valueOf(producto.getPrecio()));
                vista.txtCantidad.setText(String.valueOf(producto.getCantidad()));
            }
        } else if (e.getSource() == vista.listVendedores) {
            if (vista.listVendedores.getSelectedValue() != null) {
                Vendedor vendedor = vista.listVendedores.getSelectedValue();
                vista.txtNombreVendedor.setText(vendedor.getNombreVendedor());
                vista.txtApellidoVendedor.setText(vendedor.getApellidosVendedor());
                vista.txtDireccion.setText(vendedor.getDireccion());
                vista.fechaNacimiento.setDate(vendedor.getFecha_nacimiento());
            }
        } else if (e.getSource() == vista.listProveedores) {
            if (vista.listProveedores.getSelectedValue() != null) {
                Proveedor proveedor = vista.listProveedores.getSelectedValue();
                vista.txtNombreProveedor.setText(proveedor.getNombreProveedor());
                vista.fechaReparto.setDate(proveedor.getFechaReparto());
                vista.txtcodigoReparto.setText(proveedor.getCodigoReparto());
            }
        }
    }

    private boolean comprobarCamposProducto() {
        return !vista.txtNombreProducto.getText().isEmpty() &&
                !vista.txtOrigen.getText().isEmpty() &&
                !vista.txtReferencia.getText().isEmpty()&&
                !vista.txtPrecio.getText().isEmpty() &&
                comprobarInt(vista.txtCantidad.getText())&&
                comprobarFloat(vista.txtPrecio.getText());
    }

    private boolean comprobarCamposVendedor() {
        return !vista.txtNombreVendedor.getText().isEmpty() &&
                !vista.txtApellidoVendedor.getText().isEmpty() &&
                !vista.txtDireccion.getText().isEmpty()&&
                !vista.fechaNacimiento.getText().isEmpty();
    }

    private boolean comprobarCamposProveedor() {
        return !vista.txtNombreProveedor.getText().isEmpty()&&
                !vista.fechaReparto.getText().isEmpty()&&
                !vista.txtcodigoReparto.getText().isEmpty();
    }

    private void limpiarCamposProducto() {
        vista.txtNombreProducto.setText("");
        vista.txtOrigen.setText("");
        vista.txtReferencia.setText("");
        vista.txtPrecio.setText("");
        vista.txtCantidad.setText("");
        vista.txtBuscarProducto.setText("");
    }

    private void limpiarCamposVendedor() {
        vista.txtNombreVendedor.setText("");
        vista.txtApellidoVendedor.setText("");
        vista.txtDireccion.setText("");
        vista.fechaNacimiento.clear();
        vista.txtbuscarVendedor.setText("");
    }

    private void limpiarCamposProveedor() {
        vista.txtNombreProveedor.setText("");
        vista.fechaReparto.clear();
        vista.txtcodigoReparto.setText("");
        vista.txtBuscarProveedor.setText("");
    }


    private boolean comprobarFloat(String txt) {
        try {
            Float.parseFloat(txt);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private boolean comprobarInt(String txt) {
        try {
            Integer.parseInt(txt);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private void listarProductos() {
        vista.dlmProductos.clear();
        for (Producto producto : modelo.getProductos()) {
            vista.dlmProductos.addElement(producto);
        }
    }

    private void listarVendedores() {
        vista.dlmVendedores.clear();
        for (Vendedor vendedor : modelo.getVendedores()) {
            vista.dlmVendedores.addElement(vendedor);
        }
    }

    private void listarProveedores() {
        vista.dlmProveedores.clear();
        for (Proveedor proveedor : modelo.getProveedores()) {
            vista.dlmProveedores.addElement(proveedor);
        }
    }

    private void listarProductosBusqueda(ArrayList<Producto> lista) {
        vista.dlmProductosBusqueda.clear();
        for (Producto producto : lista) {
            vista.dlmProductosBusqueda.addElement(producto);
        }
    }

    private void listarVendedorBusqueda(ArrayList<Vendedor> lista) {
        vista.dlmVendedorBusqueda.clear();
        for (Vendedor vendedor : lista) {
            vista.dlmVendedorBusqueda.addElement(vendedor);
        }
    }

    private void listarProveedoresBusqueda(ArrayList<Proveedor> lista) {
        vista.dlmProveedoresBusqueda.clear();
        for (Proveedor proveedor : lista) {
            vista.dlmProveedoresBusqueda.addElement(proveedor);
        }
    }

    /**
     * Activa y la opcion de los botones
     * @param activados
     */

    private void setBotonesActivados(boolean activados) {
        vista.btnAnnadirProducto.setEnabled(activados);
        vista.btnModificarProducto.setEnabled(activados);
        vista.btnEliminarProducto.setEnabled(activados);
        vista.btnAnnadirVendedor.setEnabled(activados);
        vista.btnModificarVendedor.setEnabled(activados);
        vista.btnEliminarVendedor.setEnabled(activados);
        vista.btnAnnadirProveedor.setEnabled(activados);
        vista.btnModificarProveedor.setEnabled(activados);
        vista.btnEliminarProveedor.setEnabled(activados);
    }

    /**
     * Métodos innecesarios
      */

    @Override
    public void keyTyped(KeyEvent e) {}
    @Override
    public void keyPressed(KeyEvent e) {}

}
