package com.tatiana.mongo.gui;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.tatiana.mongo.base.Producto;
import com.tatiana.mongo.base.Proveedor;
import com.tatiana.mongo.base.Vendedor;
import org.bson.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Modelo {

    private MongoClient cliente;
    private MongoCollection<Document> productos;
    private MongoCollection<Document> vendedores;
    private MongoCollection<Document> proveedores;

    /**
     * Metodo para establecer conexion de la base de datos
     */

    public void conectar() {
        cliente = new MongoClient();
        String DATABASE = "Merceria";
        MongoDatabase db = cliente.getDatabase(DATABASE);

        String COLECCION_PRODUCTOS = "Productos";
        productos = db.getCollection(COLECCION_PRODUCTOS);
        String COLECCION_VENDEDORES = "Vendedores";
        vendedores = db.getCollection(COLECCION_VENDEDORES);
        String COLECCION_PROVEEDORES = "Proveedores";
        proveedores = db.getCollection(COLECCION_PROVEEDORES);
    }

    public void desconectar() {
        cliente.close();
        cliente = null;
    }

    public MongoClient getCliente() {
        return cliente;
    }

    public ArrayList<Producto> getProductos() {
        ArrayList<Producto> lista = new ArrayList<>();

        for (Document document : productos.find()) {
            lista.add(documentToProducto(document));
        }
        return lista;
    }

    public ArrayList<Producto> getProductos(String comparador) {
        ArrayList<Producto> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : productos.find(query)) {
            lista.add(documentToProducto(document));
        }

        return lista;
    }


    public ArrayList<Vendedor> getVendedores() {
        ArrayList<Vendedor> lista = new ArrayList<>();

        for (Document document : vendedores.find()) {
            lista.add(documentToVendedor(document));
        }
        return lista;
    }

    public ArrayList<Vendedor> getVendedores(String comparador) {
        ArrayList<Vendedor> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("apellidos", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : vendedores.find(query)) {
            lista.add(documentToVendedor(document));
        }

        return lista;
    }

    public ArrayList<Proveedor> getProveedores() {
        ArrayList<Proveedor> lista = new ArrayList<>();

        for (Document document : proveedores.find()) {
            lista.add(documentToProveedor(document));
        }
        return lista;
    }

    public ArrayList<Proveedor> getProveedores(String comparador) {
        ArrayList<Proveedor> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("proveedor", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : proveedores.find(query)) {
            lista.add(documentToProveedor(document));
        }

        return lista;
    }

    public void guardarObjeto(Object obj) {
        if (obj instanceof Producto) {
            productos.insertOne(objectToDocument(obj));
        } else if (obj instanceof Vendedor) {
            vendedores.insertOne(objectToDocument(obj));
        } else if (obj instanceof Proveedor) {
            proveedores.insertOne(objectToDocument(obj));
        }
    }

    public void modificarObjeto(Object obj) {
        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;
            productos.replaceOne(new Document("_id", producto.getId()), objectToDocument(producto));
        } else if (obj instanceof Vendedor) {
            Vendedor vendedor = (Vendedor) obj;
            vendedores.replaceOne(new Document("_id", vendedor.getId()), objectToDocument(vendedor));
        } else if (obj instanceof Proveedor) {
            Proveedor proveedor = (Proveedor) obj;
            proveedores.replaceOne(new Document("_id", proveedor.getId()), objectToDocument(proveedor));
        }
    }

    public void eliminarObjeto(Object obj) {
        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;
            productos.deleteOne(objectToDocument(producto));
        } else if (obj instanceof Vendedor) {
            Vendedor vendedor = (Vendedor) obj;
            vendedores.deleteOne(objectToDocument(vendedor));
        } else if (obj instanceof Proveedor) {
            Proveedor proveedor = (Proveedor) obj;
            proveedores.deleteOne(objectToDocument(proveedor));
        }
    }

    public Producto documentToProducto(Document dc) {
        Producto producto = new Producto();

        producto.setId(dc.getObjectId("_id"));
        producto.setNombreProducto(dc.getString("nombre"));
        producto.setOrigen(dc.getString("origen"));
        producto.setReferencia(dc.getString("referencia"));
        producto.setPrecio((Float.parseFloat(String.valueOf(dc.getDouble("precio")))));
        producto.setCantidad(dc.getInteger("cantidad"));
        return producto;
    }

    public Vendedor documentToVendedor(Document dc) {
        Vendedor vendedor = new Vendedor();

        vendedor.setId(dc.getObjectId("_id"));
        vendedor.setNombreVendedor(dc.getString("nombre"));
        vendedor.setApellidosVendedor(dc.getString("apellidos"));
        vendedor.setDireccion(dc.getString("direccion"));
        vendedor.setFecha_nacimiento(LocalDate.parse(dc.getString("nacimiento")));
        return vendedor;
    }

    public Proveedor documentToProveedor(Document dc) {
        Proveedor proveedor = new Proveedor();

        proveedor.setId(dc.getObjectId("_id"));
        proveedor.setNombreProveedor(dc.getString("proveedor"));
        proveedor.setFechaReparto(LocalDate.parse(dc.getString("fechaReparto")));
        proveedor.setCodigoReparto(dc.getString("codigoReparto"));
        return proveedor;
    }

    public Document objectToDocument(Object obj) {
        Document dc = new Document();

        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;

            dc.append("nombre", producto.getNombreProducto());
            dc.append("origen", producto.getOrigen());
            dc.append("referencia",producto.getReferencia());
            dc.append("precio", producto.getPrecio());
            dc.append("cantidad",producto.getCantidad());
        } else if (obj instanceof Vendedor) {
            Vendedor vendedor = (Vendedor) obj;

            dc.append("nombre", vendedor.getNombreVendedor());
            dc.append("apellidos", vendedor.getApellidosVendedor());
            dc.append("direccion", vendedor.getDireccion());
            dc.append("nacimiento", vendedor.getFecha_nacimiento().toString());

        } else if (obj instanceof Proveedor) {
            Proveedor proveedor = (Proveedor) obj;

            dc.append("proveedor", proveedor.getNombreProveedor());
            dc.append("fechaReparto",proveedor.getFechaReparto().toString());
            dc.append("codigoReparto",proveedor.getCodigoReparto());
        } else {
            return null;
        }
        return dc;
    }

}

