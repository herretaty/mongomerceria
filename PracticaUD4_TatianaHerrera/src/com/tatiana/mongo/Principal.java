package com.tatiana.mongo;

import com.tatiana.mongo.gui.Controlador;
import com.tatiana.mongo.gui.Modelo;
import com.tatiana.mongo.gui.Vista;

public class Principal {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(modelo, vista);
    }
}
