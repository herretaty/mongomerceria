package com.tatiana.mongo.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;


public class Proveedor {

    private ObjectId id;
    private String nombreProveedor;
    private LocalDate fechaReparto;
    private String codigoReparto;

    public Proveedor(String nombreProveedor, LocalDate fechaReparto, String codigoReparto){
        this.nombreProveedor=nombreProveedor;
        this.fechaReparto=fechaReparto;
        this.codigoReparto=codigoReparto;
    }

    public Proveedor() {

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }


    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public LocalDate getFechaReparto() {
        return fechaReparto;
    }

    public void setFechaReparto(LocalDate fechaReparto) {
        this.fechaReparto = fechaReparto;
    }

    public String getCodigoReparto() {
        return codigoReparto;
    }

    public void setCodigoReparto(String codigoReparto) {
        this.codigoReparto = codigoReparto;
    }

    @Override
    public String toString() {
        return "Proveedor{" +
                "Nombre Proveedor='" + nombreProveedor + '\'' +
                ", Fecha Reparto=" + fechaReparto +
                ", Codigo Reparto='" + codigoReparto + '\'' +
                '}';
    }
}
