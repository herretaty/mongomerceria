package com.tatiana.mongo.base;

import org.bson.types.ObjectId;

public class Producto {

    private ObjectId id;
    private String nombreProducto;
    private String origen;
    private String referencia;
    private float precio;
    private int cantidad;

    public Producto(String nombreProducto,String origen,String referencia, float precio, int cantidad){
        this.nombreProducto= nombreProducto;
        this.origen= origen;
        this.precio= precio;
        this.referencia= referencia;
        this.cantidad= cantidad;
    }



    public Producto() {

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getOrigen() {
        return origen;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }


    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "Nombre Producto='" + nombreProducto + '\'' +
                ", Origen='" + origen + '\'' +
                ", Referencia='" + referencia + '\'' +
                ", Precio=" + precio+ "€"+
                ", Cantidad=" + cantidad +
                '}';
    }
}
