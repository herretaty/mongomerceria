package com.tatiana.mongo.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Vendedor {
    private ObjectId id;
    private String nombreVendedor;
    private String apellidosVendedor;
    private String direccion;
    private LocalDate fecha_nacimiento;

    public Vendedor(String nombreVendedor,String apellidosVendedor,String direccion, LocalDate fecha_nacimiento){
        this.nombreVendedor= nombreVendedor;
        this.apellidosVendedor=apellidosVendedor;
        this.direccion= direccion;
        this.fecha_nacimiento= fecha_nacimiento;

    }

    public Vendedor() {

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public String getApellidosVendedor() {
        return apellidosVendedor;
    }

    public void setApellidosVendedor(String apellidosVendedor) {
        this.apellidosVendedor = apellidosVendedor;
    }


    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Vendedor{" +
                "Nombre='" + nombreVendedor + '\'' +
                ",Apellido='" + apellidosVendedor + '\'' +
                ", Direccion='" + direccion + '\'' +
                ", Fecha Nacimiento=" + fecha_nacimiento +
                '}';
    }
}
